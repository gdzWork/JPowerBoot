package top.jpower.jpower.controller.core.client;

import com.github.pagehelper.PageInfo;
import top.jpower.jpower.dbs.entity.core.client.TbCoreClient;
import top.jpower.jpower.module.annotation.Function;
import top.jpower.jpower.module.annotation.Menu;
import top.jpower.jpower.module.base.enums.JpowerError;
import top.jpower.jpower.module.base.exception.JpowerAssert;
import top.jpower.jpower.module.base.vo.Pg;
import top.jpower.jpower.module.base.vo.ResponseData;
import top.jpower.jpower.module.common.cache.CacheNames;
import top.jpower.jpower.module.common.controller.BaseController;
import top.jpower.jpower.module.common.page.PaginationContext;
import top.jpower.jpower.module.common.utils.CacheUtil;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.module.common.utils.ReturnJsonUtil;
import top.jpower.jpower.module.mp.support.Condition;
import top.jpower.jpower.service.core.client.CoreClientService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

@Api(tags = "客户端管理")
@RestController
@RequestMapping("/core/client")
@AllArgsConstructor
public class ClientController extends BaseController {

    private CoreClientService coreClientService;

    /**
     * @Author 郭丁志
     * @Description //TODO 保存或者更新客户端信息
     * @Date 14:45 2020-07-31
     * @Param [coreClient]
     * @return top.jpower.jpower.module.base.vo.ResponseData
     **/
    @Function(value = "保存",menus = {
            @Menu(name = "编辑",client = "admin",menuCode = "SYSTEM_CLIENT",code = "SYSTEM_CLIENT_SAVE",type = Menu.TYPE.BTN),
            @Menu(name = "新增",client = "admin",menuCode = "SYSTEM_CLIENT",code = "SYSTEM_CLIENT_ADD",type = Menu.TYPE.BTN)
    })
    @ApiOperation("保存或者更新客户端信息")
    @PostMapping("save")
    public ResponseData save(TbCoreClient coreClient){

        if (Fc.isNull(coreClient.getId())){
            JpowerAssert.notEmpty(coreClient.getClientCode(), JpowerError.Arg,"客户端Code不可为空");
            JpowerAssert.notEmpty(coreClient.getName(), JpowerError.Arg,"客户端名称不可为空");
            JpowerAssert.notTrue(coreClient.getRefreshTokenValidity() <= coreClient.getAccessTokenValidity(),JpowerError.Arg,"刷新令牌时长不可小于令牌时长");


            if (coreClientService.count(Condition.<TbCoreClient>getQueryWrapper().lambda().eq(TbCoreClient::getClientCode,coreClient.getClientCode())) > 0){
                return ReturnJsonUtil.busFail("该客户端已存在");
            }
        }else {
            //防止用户A在更新时，用户B做了删除操作
            TbCoreClient client =coreClientService.getById(coreClient.getId());
            if (Fc.isNull(client)){
                return ReturnJsonUtil.fail("该数据不存在");
            }

            long refreshTokenValidity = Fc.isNull(coreClient.getRefreshTokenValidity())?client.getRefreshTokenValidity():coreClient.getRefreshTokenValidity();
            long accessTokenValidity = Fc.isNull(coreClient.getAccessTokenValidity())?client.getAccessTokenValidity():coreClient.getAccessTokenValidity();
            JpowerAssert.notTrue(refreshTokenValidity <= accessTokenValidity,JpowerError.Arg,"刷新令牌时长不可小于令牌时长");

            if (Fc.notNull(coreClient.getId())){
                TbCoreClient tbCoreClient = coreClientService.getOne(Condition.<TbCoreClient>getQueryWrapper().lambda().eq(TbCoreClient::getClientCode,coreClient.getClientCode()));
                if (!Fc.isNull(tbCoreClient) && !Fc.equals(tbCoreClient.getId(),client.getId())){
                    return ReturnJsonUtil.busFail("该客户端已存在");
                }
            }

        }

        CacheUtil.clear(CacheNames.CLIENT_KEY,Boolean.FALSE);
        return ReturnJsonUtil.status(coreClientService.saveOrUpdate(coreClient));
    }

    @Function(value = "删除",menus = {
            @Menu(client = "admin",menuCode = "SYSTEM_CLIENT",code = "SYSTEM_CLIENT_DELETE",type = Menu.TYPE.BTN)
    })
    @ApiOperation("删除客户端")
    @DeleteMapping("delete")
    public ResponseData delete(@ApiParam(value = "主键，多个逗号分割",required = true) @RequestParam String ids){
        JpowerAssert.notEmpty(ids,JpowerError.Arg,"客户端主键不可为空");
        CacheUtil.clear(CacheNames.CLIENT_KEY,Boolean.FALSE);
        return ReturnJsonUtil.status(coreClientService.removeByIds(Fc.toLongList(ids)));
    }

    @Function(value = "列表",menus = {
            @Menu(client = "admin",menuCode = "SYSTEM_CLIENT",code = "CLIENT_LIST",type = Menu.TYPE.INTERFACE)
    })
    @ApiOperation("分页查询客户端列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum",value = "第几页",defaultValue = "1",paramType = "query",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "pageSize",value = "每页长度",defaultValue = "10",paramType = "query",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "name",value = "客户端名称",paramType = "query"),
            @ApiImplicitParam(name = "clientCode",value = "客户端编码",paramType = "query")
    })
    @GetMapping("list")
    public ResponseData<Pg<TbCoreClient>> list(@ApiIgnore @RequestParam Map<String,Object> coreClient){
        PaginationContext.startPage();
        List<TbCoreClient> list = coreClientService.list(Condition.getQueryWrapper(coreClient,TbCoreClient.class).lambda().orderByAsc(TbCoreClient::getSortNum));
        return ReturnJsonUtil.ok("查询成功",new PageInfo<>(list));
    }

    @Function(value = "客户端下拉",menus = {
            @Menu(client = "admin",menuCode = "SYSTEM_FUNCTION",code = "FUNCTION_CLIENT_SELECT",type = Menu.TYPE.INTERFACE),
            @Menu(client = "admin",menuCode = "SYSTEM_ROLE",btnCode = "SYSTEM_ROLE_SELECT_URL",code = "ROLE_CLIENT_SELECT",type = Menu.TYPE.INTERFACE),
            @Menu(client = "admin",menuCode = "SYSTEM_DATASCOPE",code = "DATASCOPE_CLIENT_SELECT",type = Menu.TYPE.INTERFACE),
            @Menu(client = "admin",menuCode = "SYSTEM_TOPMENU",code = "TOPMENU_CLIENT_SELECT",type = Menu.TYPE.INTERFACE)
    })
    @ApiOperation("下拉客户端列表")
    @GetMapping("selectList")
    public ResponseData<List<Map<String,Object>>> selectList(){

        return ReturnJsonUtil.data(coreClientService.listMaps(Condition.<TbCoreClient>getQueryWrapper()
                .lambda()
                .select(TbCoreClient::getId,TbCoreClient::getName)
                .orderByAsc(TbCoreClient::getSortNum)));
    }
}
