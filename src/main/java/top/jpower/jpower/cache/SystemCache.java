package top.jpower.jpower.cache;

import top.jpower.jpower.dbs.entity.core.city.TbCoreCity;
import top.jpower.jpower.dbs.entity.core.client.TbCoreClient;
import top.jpower.jpower.dbs.entity.core.function.TbCoreDataScope;
import top.jpower.jpower.dbs.entity.core.function.TbCoreFunction;
import top.jpower.jpower.dbs.entity.core.org.TbCoreOrg;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRole;
import top.jpower.jpower.dbs.entity.core.tenant.TbCoreTenant;
import top.jpower.jpower.module.common.cache.CacheNames;
import top.jpower.jpower.module.common.utils.CacheUtil;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.module.common.utils.SpringUtil;
import top.jpower.jpower.module.common.utils.constants.StringPool;
import top.jpower.jpower.module.mp.support.Condition;
import top.jpower.jpower.service.core.city.CoreCityService;
import top.jpower.jpower.service.core.city.impl.CoreCityServiceImpl;
import top.jpower.jpower.service.core.client.CoreClientService;
import top.jpower.jpower.service.core.client.impl.CoreClientServiceImpl;
import top.jpower.jpower.service.core.org.CoreOrgService;
import top.jpower.jpower.service.core.org.impl.CoreOrgServiceImpl;
import top.jpower.jpower.service.core.role.CoreDataScopeService;
import top.jpower.jpower.service.core.role.CoreFunctionService;
import top.jpower.jpower.service.core.role.CoreRoleService;
import top.jpower.jpower.service.core.role.impl.CoreDataScopeServiceImpl;
import top.jpower.jpower.service.core.role.impl.CoreFunctionServiceImpl;
import top.jpower.jpower.service.core.tenant.TenantService;
import top.jpower.jpower.service.core.tenant.impl.TenantServiceImpl;

import java.util.List;

/**
 * @ClassName SystemCache
 * @Description TODO 系统缓存
 * @Author 郭丁志
 * @Date 2020-05-06 14:55
 * @Version 1.0
 */
public class SystemCache {

    private static CoreOrgService coreOrgService;
    private static CoreClientService coreClientService;
    private static CoreFunctionService coreFunctionService;
    private static TenantService tenantService;
    private static CoreDataScopeService coreDataScopeService;
    private static CoreRoleService coreRoleService;
    private static CoreCityService coreCityService;

    static {
        coreOrgService = SpringUtil.getBean(CoreOrgServiceImpl.class);
        coreClientService = SpringUtil.getBean(CoreClientServiceImpl.class);
        coreFunctionService = SpringUtil.getBean(CoreFunctionServiceImpl.class);
        tenantService = SpringUtil.getBean(TenantServiceImpl.class);
        coreDataScopeService = SpringUtil.getBean(CoreDataScopeServiceImpl.class);
        coreCityService = SpringUtil.getBean(CoreCityServiceImpl.class);
        coreRoleService = SpringUtil.getBean(CoreRoleService.class);
    }

    /**
     * @Author 郭丁志
     * @Description //TODO 获取部门名称
     * @Date 15:47 2020-05-06
     **/
    public static String getOrgName(Long orgId){
        TbCoreOrg org = getOrg(orgId);
        if (Fc.isNull(org)){
            return StringPool.EMPTY;
        }
        return org.getName();
    }

    /**
     * @Author 郭丁志
     * @Description //TODO 获取部门
     * @Date 15:47 2020-05-06
     **/
    public static TbCoreOrg getOrg(Long orgId){
        return CacheUtil.get(CacheNames.ORG_KEY, CacheNames.ORG_DETAIL_KEY,orgId,() -> {
            TbCoreOrg org = coreOrgService.getById(orgId);
            return org;
        });
    }

    /**
     * @Author 郭丁志
     * @Description //TODO 根据部门ID获取下级所有ID
     * @Date 15:47 2020-05-06
     **/
    public static List<Long> getChildIdOrgById(Long orgId) {
        return CacheUtil.get(CacheNames.ORG_KEY, CacheNames.ORG_CHILDID_KEY,orgId,() -> {
            List<Long> responseData = coreOrgService.queryChildById(orgId);
            return responseData;
        });
    }

    /**
     * @Author 郭丁志
     * @Description //TODO 获取客户端信息
     * @Date 15:47 2020-05-06
     **/
    public static TbCoreClient getClientByClientCode(String clientCode) {
        return CacheUtil.get(CacheNames.CLIENT_KEY, CacheNames.CLIENTCODE_KEY,clientCode,() -> {
            TbCoreClient responseData = coreClientService.loadClientByClientCode(clientCode);
            return responseData;
        },Boolean.FALSE);
    }

    /**
     * 通过角色ID查询指定客户端的接口URL
     *
     * @author mr.g
     * @param roleIds 角色ID
     * @return URL列表
     **/
    public static List<String> getUrlsByRoleIds(List<Long> roleIds, String clientCode) {
        return CacheUtil.get(CacheNames.FUNCTION_KEY, CacheNames.URL_CLIENT_ROLE_KEY,clientCode+StringPool.COLON+roleIds,() -> {
            List<String> responseData = coreFunctionService.getUrlsByRoleIds(roleIds,clientCode);
            return responseData;
        });
    }

    /**
     * @author 郭丁志
     * @Description //TODO 获取租户信息
     * @date 17:38 2020/10/25 0025
     * @param tenantCode 租户编码
     * @return top.jpower.jpower.dbs.entity.tenant.TbCoreTenant
     */
    public static TbCoreTenant getTenantByCode(String tenantCode) {
        return CacheUtil.get(CacheNames.TENANT_KEY, CacheNames.TENANT_CODE_KEY,tenantCode,() -> {
            TbCoreTenant responseData = tenantService.getOne(Condition.<TbCoreTenant>getQueryWrapper().lambda().eq(TbCoreTenant::getTenantCode,tenantCode));
            return responseData;
        });
    }

    /**
     * 通过角色ID获取所有菜单
     *
     * @author 郭丁志
     * @date 23:28 2020/11/5 0005
     * @param roleIds 角色ID
     */
    public static List<TbCoreFunction> getMenuListByRole(List<Long> roleIds, String clientCode) {
        return CacheUtil.get(CacheNames.FUNCTION_KEY, CacheNames.MENU_CLIENT_ROLE_KEY,clientCode+StringPool.COLON+roleIds,() -> {
            List<TbCoreFunction> responseData = coreFunctionService.listMenuByRoleId(roleIds, clientCode, null, Boolean.FALSE);
            return responseData;
        });
    }

    /**
     * 通过角色ID获取指定客户端的所有菜单
     *
     * @author 郭丁志
     * @date 23:38 2020/11/5 0005
     * @param roleIds  角色ID
     * @return java.util.List<top.jpower.jpower.dbs.entity.function.TbCoreDataScope>
     */
    public static List<String> getRoleNameByIds(List<Long> roleIds) {
        return CacheUtil.get(CacheNames.ROLE_KEY, CacheNames.ROLENAME_KEY,roleIds,() -> {
            List<String> responseData = coreRoleService.listObjs(Condition.<TbCoreRole>getQueryWrapper()
                    .lambda().select(TbCoreRole::getName).in(TbCoreRole::getId,roleIds), Fc::toStr);
            return responseData;
        });
    }

    /**
     * 查询可所有角色执行得数据权限
     *
     * @author 郭丁志
     * @date 23:31 2020/11/5 0005
     * @return java.util.List<top.jpower.jpower.dbs.entity.function.TbCoreDataScope>
     */
    public static List<TbCoreDataScope> getAllRoleDataScope() {
        return CacheUtil.get(CacheNames.DATASCOPE_KEY, CacheNames.DATASCOPE_ALLROLE_KEY,"all",() -> {
            List<TbCoreDataScope> responseData = coreDataScopeService.getAllRoleDataScope();
            return responseData;
        },Boolean.FALSE);
    }

    /**
     * 根据角色ID获取指定客户端的数据权限
     *
     * @author 郭丁志
     * @date 23:38 2020/11/5 0005
     * @param roleIds  角色ID
     * @return java.util.List<top.jpower.jpower.dbs.entity.function.TbCoreDataScope>
     */
    public static List<TbCoreDataScope> getDataScopeByRole(List<Long> roleIds,String clientCode) {
        return CacheUtil.get(CacheNames.DATASCOPE_KEY, CacheNames.DATASCOPE_CLIENT_ROLE_KEY,clientCode+StringPool.COLON+roleIds,() -> {
            List<TbCoreDataScope> responseData = coreDataScopeService.getDataScopeByRole(roleIds,clientCode);
            return responseData;
        });
    }

    /**
     * 获取地区名称
     *
     * @Author ding
     * @Date 23:38 2021-02-21
     * @param code
     * @return java.lang.String
     **/
    public static String getCityName(String code) {
        TbCoreCity city = getCity(code);
        if (Fc.isNull(city)){
            return StringPool.EMPTY;
        }
        return city.getName();
    }

    /**
     * 获取地区
     * @Author ding
     * @Date 23:39 2021-02-21
     * @param code
     * @return top.jpower.jpower.dbs.entity.city.TbCoreCity
     **/
    public static TbCoreCity getCity(String code) {
        return CacheUtil.get(CacheNames.CITY_KEY, CacheNames.CITY_CODE_KEY,code,() -> {
            TbCoreCity responseData = coreCityService.queryByCode(code);
            return responseData;
        },Boolean.FALSE);
    }
}
