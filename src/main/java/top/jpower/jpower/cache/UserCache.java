package top.jpower.jpower.cache;

import top.jpower.jpower.dbs.entity.core.user.TbCoreUser;
import top.jpower.jpower.module.common.cache.CacheNames;
import top.jpower.jpower.module.common.utils.CacheUtil;
import top.jpower.jpower.module.common.utils.SpringUtil;
import top.jpower.jpower.service.core.user.CoreUserRoleService;
import top.jpower.jpower.service.core.user.CoreUserService;
import top.jpower.jpower.service.core.user.impl.CoreUserRoleServiceImpl;
import top.jpower.jpower.service.core.user.impl.CoreUserServiceImpl;
import top.jpower.jpower.vo.core.UserVo;

import java.util.List;

/**
 * @ClassName UserCache
 * @Description TODO 用户缓存
 * @Author 郭丁志
 * @Version 1.0
 */
public class UserCache {

    private static CoreUserService userService;
    private static CoreUserRoleService userRoleService;

    static {
        userService = SpringUtil.getBean(CoreUserServiceImpl.class);
        userRoleService = SpringUtil.getBean(CoreUserRoleServiceImpl.class);
    }

    public static TbCoreUser getUserByPhone(String telephone, String tenantCode) {
        return CacheUtil.get(CacheNames.USER_KEY, CacheNames.USER_PHPNE_KEY,telephone,() -> {
            TbCoreUser user = userService.selectByPhone(telephone,tenantCode);
            return user;
        });
    }

    public static TbCoreUser getUserByLoginId(String loginId, String tenantCode) {
        return CacheUtil.get(CacheNames.USER_KEY, CacheNames.USER_LOGINID_KEY,loginId,() -> {
            TbCoreUser user = userService.selectUserLoginId(loginId,tenantCode);
            return user;
        });
    }

    public static List<Long> getRoleIds(Long userId) {
        return CacheUtil.get(CacheNames.USER_KEY, CacheNames.USER_ROLEID_KEY,userId,() -> {
            List<Long> list = userRoleService.queryRoleIds(userId);
            return list;
        });
    }

    public static TbCoreUser getUserByCode(String otherCode, String tenantCode) {
        return CacheUtil.get(CacheNames.USER_KEY, CacheNames.USER_OTHERCODE_KEY,otherCode,() -> {
            TbCoreUser user = userService.selectUserByOtherCode(otherCode,tenantCode);
            return user;
        });
    }

    public static UserVo getById(Long userId) {
        return CacheUtil.get(CacheNames.USER_KEY, CacheNames.USER_DETAIL_KEY,userId,() -> userService.selectUserById(userId));
    }
}
