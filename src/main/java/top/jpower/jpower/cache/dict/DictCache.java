package top.jpower.jpower.cache.dict;

import top.jpower.jpower.module.common.cache.CacheNames;
import top.jpower.jpower.module.common.utils.*;
import top.jpower.jpower.module.common.utils.constants.ConstantsEnum;
import top.jpower.jpower.module.common.utils.constants.StringPool;
import top.jpower.jpower.service.core.dict.CoreDictService;
import top.jpower.jpower.service.core.dict.impl.CoreDictServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static top.jpower.jpower.module.common.utils.constants.ConstantsUtils.I18N_KEY;

/**
 * @ClassName DictConfig
 * @Description TODO 获取字典
 * @Author 郭丁志
 * @Date 2020-05-06 14:55
 * @Version 1.0
 */
public class DictCache {

    private static CoreDictService dictClient;

    static {
        dictClient = SpringUtil.getBean(CoreDictServiceImpl.class);
    }

    /**
     * @Author 郭丁志
     * @Description //TODO 通过字典类型和编码直接返回值
     * @Date 17:28 2020-10-22
     * @Param [dictTypeCode, code]
     **/
    public static String getDictByTypeAndCode(String dictTypeCode, String code) {
        List<Map<String, Object>> list = getDictByType(dictTypeCode);
        list = Fc.isNull(list)?new ArrayList<>():list;
        return list.stream()
                .filter(map -> {

                    String requestLocale = ConstantsEnum.YYZL.CHINA.getValue();
                    if (Fc.notNull(WebUtil.getRequest())){
                        requestLocale = Fc.toStr(Objects.requireNonNull(WebUtil.getRequest()).getHeader(I18N_KEY), ConstantsEnum.YYZL.CHINA.getValue());
                    }

                    return Fc.equalsValue(top.jpower.jpower.module.common.utils.MapUtil.getStr(map,"code"),code) && Fc.equalsValue(top.jpower.jpower.module.common.utils.MapUtil.getStr(map,"locale"), requestLocale);
                })
                .map(map-> MapUtil.getStr(map,"name"))
                .collect(Collectors.joining(StringPool.SPILT));
    }

    /**
     * @Author 郭丁志
     * @Description //TODO 通过字典类型查询字典列表
     * @Date 17:29 2020-10-22
     * @Param [dictTypeCode]
     **/
    public static List<Map<String, Object>> getDictByType(String dictTypeCode) {
        return CacheUtil.get(CacheNames.DICT_KEY,CacheNames.DICT_TYPE_KEY,dictTypeCode,() -> {
            List<Map<String, Object>> responseData = dictClient.listByTypeCode(dictTypeCode);
            return responseData;
        });
    }
}