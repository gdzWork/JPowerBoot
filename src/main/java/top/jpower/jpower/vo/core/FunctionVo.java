package top.jpower.jpower.vo.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import top.jpower.jpower.dbs.entity.core.function.TbCoreFunction;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName Function
 * @Description TODO 菜单返回试图
 * @Author 郭丁志
 * @Date 2020-07-30 10:49
 * @Version 1.0
 */
@Data
public class FunctionVo extends TbCoreFunction {

    @ApiModelProperty("页面打开方式")
    private String targetStr;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Boolean hasChildren;

}
