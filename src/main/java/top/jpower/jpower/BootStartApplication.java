package top.jpower.jpower;

import top.jpower.jpower.module.common.deploy.JpowerApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ClassName SpringBootStartApplication
 * @Author 郭丁志
 * @Date 2020-02-24 18:41
 * @Version 1.0
 */
@SpringBootApplication
@EnableTransactionManagement
public class BootStartApplication {

    public static void main(String[] args) {
        JpowerApplication.run("jpower-boot", BootStartApplication.class, args);
    }
}
