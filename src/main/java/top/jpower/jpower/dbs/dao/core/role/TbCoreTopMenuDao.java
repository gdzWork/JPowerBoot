package top.jpower.jpower.dbs.dao.core.role;

import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreTopMenuMapper;
import top.jpower.jpower.dbs.entity.core.function.TbCoreTopMenu;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author mr.g
 */
@Repository
public class TbCoreTopMenuDao extends JpowerServiceImpl<TbCoreTopMenuMapper, TbCoreTopMenu> {
}
