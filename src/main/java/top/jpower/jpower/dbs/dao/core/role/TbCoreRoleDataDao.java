package top.jpower.jpower.dbs.dao.core.role;

import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreRoleDataMapper;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleData;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author ding
 * @description
 * @date 2020-11-03 14:59
 */
@Repository
public class TbCoreRoleDataDao extends JpowerServiceImpl<TbCoreRoleDataMapper, TbCoreRoleData> {
}
