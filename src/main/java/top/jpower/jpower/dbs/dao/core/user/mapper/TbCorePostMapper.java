package top.jpower.jpower.dbs.dao.core.user.mapper;

import top.jpower.jpower.dbs.entity.core.user.TbCorePost;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author mr.g
 * @date 2022-09-16 17:57
 */
@Component("corePostMapper")
public interface TbCorePostMapper extends JpowerBaseMapper<TbCorePost> {
}
