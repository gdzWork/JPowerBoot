package top.jpower.jpower.dbs.dao.core.role.mapper;

import top.jpower.jpower.dbs.entity.core.role.TbCoreRole;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author mr.gmac
 */
@Component("tbCoreRoleMapper")
public interface TbCoreRoleMapper extends JpowerBaseMapper<TbCoreRole> {
}
