package top.jpower.jpower.dbs.dao.core.role.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import top.jpower.jpower.dbs.entity.core.function.TbCoreFunction;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import top.jpower.jpower.vo.core.DataFunctionVo;
import top.jpower.jpower.vo.core.FunctionVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author mr.gmac
 */
@Component("tbCoreFunctionMapper")
public interface TbCoreFunctionMapper extends JpowerBaseMapper<TbCoreFunction> {

    /**
     * 查功能列表
     * @Author goo
     * @Date 16:47 2021-02-17
     * @param coreFunction
     * @param functionType
     * @return java.util.List<top.jpower.jpower.dbs.entity.function.TbCoreFunction>
     **/
    List<FunctionVo> listFunction(@Param(Constants.WRAPPER) Wrapper<TbCoreFunction> coreFunction,@Param("functionType") String functionType);

    List<DataFunctionVo> listDataFunction(@Param(Constants.WRAPPER) LambdaQueryWrapper<TbCoreFunction> wrapper);
}
