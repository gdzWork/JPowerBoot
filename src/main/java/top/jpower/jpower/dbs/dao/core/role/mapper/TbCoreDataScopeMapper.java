package top.jpower.jpower.dbs.dao.core.role.mapper;

import top.jpower.jpower.dbs.entity.core.function.TbCoreDataScope;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author mr.gmac
 * @description
 * @date 2020-11-03 14:50
 */
@Component
public interface TbCoreDataScopeMapper extends JpowerBaseMapper<TbCoreDataScope> {
}
