package top.jpower.jpower.dbs.dao.core.role;


import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreRoleMapper;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRole;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author mr.gmac
 */
@Repository
public class TbCoreRoleDao extends JpowerServiceImpl<TbCoreRoleMapper, TbCoreRole> {
}
