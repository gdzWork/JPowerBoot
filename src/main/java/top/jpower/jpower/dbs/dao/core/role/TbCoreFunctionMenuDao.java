package top.jpower.jpower.dbs.dao.core.role;

import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreFunctionMenuMapper;
import top.jpower.jpower.dbs.entity.core.function.TbCoreFunctionMenu;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author mr.g
 */
@Repository
public class TbCoreFunctionMenuDao extends JpowerServiceImpl<TbCoreFunctionMenuMapper, TbCoreFunctionMenu> {
}
