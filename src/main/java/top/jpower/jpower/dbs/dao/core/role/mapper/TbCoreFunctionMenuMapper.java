package top.jpower.jpower.dbs.dao.core.role.mapper;

import top.jpower.jpower.dbs.entity.core.function.TbCoreFunctionMenu;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author mr.g
 * @date 2022/10/23 23:26
 */
@Component
public interface TbCoreFunctionMenuMapper extends JpowerBaseMapper<TbCoreFunctionMenu> {
}
