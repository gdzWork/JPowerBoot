package top.jpower.jpower.dbs.dao.core.role.mapper;

import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleData;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author ding
 * @description
 * @date 2020-11-03 14:58
 */
@Component
public interface TbCoreRoleDataMapper extends JpowerBaseMapper<TbCoreRoleData> {
}
