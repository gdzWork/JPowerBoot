package top.jpower.jpower.dbs.dao.core.user.mapper;

import top.jpower.jpower.dbs.entity.core.user.TbCoreUser;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author mr.gmac
 */
@Component("tbCoreUserMapper")
public interface TbCoreUserMapper extends JpowerBaseMapper<TbCoreUser> {

    TbCoreUser selectAllById(Long id);

    List<TbCoreUser> selectUserList(@Param("coreUser") TbCoreUser coreUser, @Param("orgIds") List<Long> orgIds);
}
