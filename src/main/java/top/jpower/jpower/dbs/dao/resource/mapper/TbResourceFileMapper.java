package top.jpower.jpower.dbs.dao.resource.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.jpower.jpower.dbs.entity.resource.TbResourceFile;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;


/**
 * @ClassName TbCoreParamsDao
 * @Description TODO 文件管理
 * @Author 郭丁志
 * @Date 2020-07-03 13:30
 * @Version 1.0
 */
@Mapper
public interface TbResourceFileMapper extends JpowerBaseMapper<TbResourceFile> {

}
