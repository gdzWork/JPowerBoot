package top.jpower.jpower.dbs.dao.core.tenant.mapper;

import top.jpower.jpower.dbs.entity.core.tenant.TbCoreTenant;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

@Component
public interface TbCoreTenantMapper extends JpowerBaseMapper<TbCoreTenant> {
}
