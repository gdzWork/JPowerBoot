package top.jpower.jpower.dbs.dao.core.role.mapper;

import top.jpower.jpower.dbs.entity.core.function.TbCoreTopMenu;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author mr.g
 * @date 2022/10/23 23:25
 */
@Component
public interface TbCoreTopMenuMapper extends JpowerBaseMapper<TbCoreTopMenu> {
}
