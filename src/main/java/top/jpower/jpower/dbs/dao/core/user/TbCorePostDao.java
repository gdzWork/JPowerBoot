package top.jpower.jpower.dbs.dao.core.user;

import top.jpower.jpower.dbs.dao.core.user.mapper.TbCorePostMapper;
import top.jpower.jpower.dbs.dao.core.user.mapper.TbCoreUserMapper;
import top.jpower.jpower.dbs.entity.core.user.TbCorePost;
import top.jpower.jpower.dbs.entity.core.user.TbCoreUser;
import top.jpower.jpower.module.common.utils.BeanUtil;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.module.dbs.dao.BaseDaoWrapper;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import top.jpower.jpower.module.mp.support.Condition;
import top.jpower.jpower.vo.core.PostVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

/**
 * @author mr.g
 * @date 2022-09-16 17:58
 */
@Repository
@RequiredArgsConstructor
public class TbCorePostDao extends JpowerServiceImpl<TbCorePostMapper, TbCorePost> implements BaseDaoWrapper<TbCorePost, PostVo> {

    private final TbCoreUserMapper userMapper;

    @Override
    public PostVo conver(TbCorePost post) {

        if (Fc.notNull(post)) {
            PostVo postVo = BeanUtil.copyProperties(post,PostVo.class);
            postVo.setUserNum(userMapper.selectCount(Condition.<TbCoreUser>getQueryWrapper()
                    .lambda().eq(TbCoreUser::getPostId,post.getId())));
            return postVo;
        }

        return null;
    }
}
