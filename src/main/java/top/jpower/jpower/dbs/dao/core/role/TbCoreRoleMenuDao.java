package top.jpower.jpower.dbs.dao.core.role;

import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreRoleMenuMapper;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleMenu;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author mr.g
 */
@Repository
public class TbCoreRoleMenuDao extends JpowerServiceImpl<TbCoreRoleMenuMapper, TbCoreRoleMenu> {
}
