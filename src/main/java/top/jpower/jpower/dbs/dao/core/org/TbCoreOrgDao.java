package top.jpower.jpower.dbs.dao.core.org;


import top.jpower.jpower.dbs.dao.core.org.mapper.TbCoreOrgMapper;
import top.jpower.jpower.dbs.entity.core.org.TbCoreOrg;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author mr.gmac
 */
@Repository
public class TbCoreOrgDao extends JpowerServiceImpl<TbCoreOrgMapper, TbCoreOrg> {
}
