package top.jpower.jpower.dbs.dao.core.dict;

import top.jpower.jpower.dbs.dao.core.dict.mapper.TbCoreDictTypeMapper;
import top.jpower.jpower.dbs.entity.core.dict.TbCoreDictType;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @ClassName TbCoreParamsDao
 * @Description TODO
 * @Author 郭丁志
 * @Date 2020-07-03 13:30
 * @Version 1.0
 */
@Repository
public class TbCoreDictTypeDao extends JpowerServiceImpl<TbCoreDictTypeMapper, TbCoreDictType> {


}
