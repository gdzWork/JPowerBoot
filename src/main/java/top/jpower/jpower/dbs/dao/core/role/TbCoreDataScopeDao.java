package top.jpower.jpower.dbs.dao.core.role;


import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreDataScopeMapper;
import top.jpower.jpower.dbs.entity.core.function.TbCoreDataScope;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @description
 * @author ding
 * @date 2020-11-03 14:53
 */
@Repository
public class TbCoreDataScopeDao extends JpowerServiceImpl<TbCoreDataScopeMapper, TbCoreDataScope> {
}
