package top.jpower.jpower.dbs.dao.log.mapper;

import top.jpower.jpower.dbs.entity.log.TbLogOperate;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @Author mr.g
 * @Date 2021/5/1 0001 19:39
 */
@Component
public interface LogOperateMapper extends JpowerBaseMapper<TbLogOperate> {
}
