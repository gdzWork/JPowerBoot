package top.jpower.jpower.dbs.dao.core.user;


import top.jpower.jpower.dbs.dao.core.user.mapper.TbCoreUserRoleMapper;
import top.jpower.jpower.dbs.entity.core.user.TbCoreUserRole;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author mr.gmac
 */
@Repository
public class TbCoreUserRoleDao extends JpowerServiceImpl<TbCoreUserRoleMapper, TbCoreUserRole> {
}
