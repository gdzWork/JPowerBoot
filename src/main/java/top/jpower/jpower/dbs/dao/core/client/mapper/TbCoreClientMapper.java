package top.jpower.jpower.dbs.dao.core.client.mapper;

import top.jpower.jpower.dbs.entity.core.client.TbCoreClient;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author mr.gmac
 */
@Component
public interface TbCoreClientMapper extends JpowerBaseMapper<TbCoreClient> {
}
