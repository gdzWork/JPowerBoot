package top.jpower.jpower.dbs.dao.resource.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.jpower.jpower.dbs.entity.resource.TbResourceOss;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;

/**
 * @author mr.g
 * @date 2024/4/23 10:14 AM
 */
@Mapper
public interface TbResourceOssMapper extends JpowerBaseMapper<TbResourceOss> {
}
