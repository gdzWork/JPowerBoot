package top.jpower.jpower.dbs.dao.core.dict;

import top.jpower.jpower.dbs.dao.core.dict.mapper.TbCoreDictMapper;
import top.jpower.jpower.dbs.entity.core.dict.TbCoreDict;
import top.jpower.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @ClassName TbCoreParamsDao
 * @Description TODO
 * @Author 郭丁志
 * @Date 2020-07-03 13:30
 * @Version 1.0
 */
@Repository
public class TbCoreDictDao extends JpowerServiceImpl<TbCoreDictMapper, TbCoreDict> {


}
