package top.jpower.jpower.dbs.dao.core.city.mapper;

import top.jpower.jpower.dbs.entity.core.city.TbCoreCity;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @ClassName TbCoreParamsDao
 * @Description TODO
 * @Author 郭丁志
 * @Date 2020-07-03 13:30
 * @Version 1.0
 */
@Component("tbCoreCityMapper")
public interface TbCoreCityMapper extends JpowerBaseMapper<TbCoreCity> {

}
