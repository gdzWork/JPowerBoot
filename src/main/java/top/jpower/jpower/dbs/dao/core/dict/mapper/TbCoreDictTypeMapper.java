package top.jpower.jpower.dbs.dao.core.dict.mapper;

import top.jpower.jpower.dbs.entity.core.dict.TbCoreDictType;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @ClassName TbCoreParamsDao
 * @Description TODO
 * @Author 郭丁志
 * @Date 2020-07-03 13:30
 * @Version 1.0
 */
@Component("tbCoreDictTypeMapper")
public interface TbCoreDictTypeMapper extends JpowerBaseMapper<TbCoreDictType> {


}
