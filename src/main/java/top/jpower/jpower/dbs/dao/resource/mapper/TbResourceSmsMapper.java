package top.jpower.jpower.dbs.dao.resource.mapper;

import top.jpower.jpower.dbs.entity.resource.TbResourceSms;
import top.jpower.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;

/**
 * <p>
 * 短信配置表 Mapper 接口
 * </p>
 *
 * @author mr.g
 * @since 2024-03-04
 */
public interface TbResourceSmsMapper extends JpowerBaseMapper<TbResourceSms> {

}
