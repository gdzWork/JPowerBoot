package top.jpower.jpower.dbs.entity.log;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.jpower.jpower.module.base.annotation.Dict;

/**
 * @Author mr.g
 * @Date 2021/5/1 0001 18:59
 */
@Data
public class TbLogOperate extends TbLogBase {

    private static final long serialVersionUID = -8822380359274895613L;

    @ApiModelProperty("操作标题")
    private String title;
    @ApiModelProperty("业务类型（OTHER=其它,INSERT=新增,UPDATE=修改,DELETE=删除,GRANT=授权,EXPORT=导出,IMPORT=导入,FORCE=强退,GENCODE=生成代码,CLEAN=清空数据,REVIEW=审核）")
    @Dict(name = "BUSINESS_TYPE",attributes = "businessTypeStr")
    private String businessType;
    @ApiModelProperty("返回内容")
    private String returnContent;
    @ApiModelProperty("操作状态（0正常 1异常）")
    @Dict(name = "OPERATE_STATUS",attributes = "statusStr")
    private Integer status;
    @ApiModelProperty("错误消息")
    private String errorMsg;
    @ApiModelProperty("记录ID")
    private String recordId;
    @ApiModelProperty("记录内容")
    private String content;


    @TableField(exist = false)
    private String statusStr;
    @TableField(exist = false)
    private String businessTypeStr;
}
