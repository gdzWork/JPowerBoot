package top.jpower.jpower.builder.auth.granter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.jpower.jpower.builder.auth.AuthUserInfo;
import top.jpower.jpower.builder.auth.TokenGranter;
import top.jpower.jpower.builder.auth.TokenParameter;
import top.jpower.jpower.cache.UserCache;
import top.jpower.jpower.config.sms.SmsBuilder;
import top.jpower.jpower.dbs.entity.core.user.TbCoreUser;
import top.jpower.jpower.module.base.exception.BusinessException;
import top.jpower.jpower.module.common.auth.UserInfo;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.utils.TokenUtil;
import top.jpower.jpower.utils.UserUtil;

import static top.jpower.jpower.builder.auth.granter.PhoneTokenGranter.GRANT_TYPE;
import static top.jpower.jpower.module.common.utils.constants.JpowerConstants.VALIDATE_SMS_CODE;


/**
 * @ClassName PhoneTokenGranter
 * @Description TODO 手机号登录默认实现类
 * @Author 郭丁志
 * @Date 2020-07-28 14:23
 * @Version 1.0
 */
@Component(GRANT_TYPE)
public class PhoneTokenGranter implements TokenGranter {

    public static final String GRANT_TYPE = "phone";

    @Autowired(required = false)
    private AuthUserInfo authUserInfo;
    @Autowired
    private SmsBuilder smsBuilder;

    @Override
    public UserInfo grant(TokenParameter tokenParameter) {
        String phone = tokenParameter.getPhone();
        String phoneCode = tokenParameter.getPhoneCode();
        String tenantCode = tokenParameter.getTenantCode();
        if (!smsBuilder.getTemplate(VALIDATE_SMS_CODE).validate(phone, phoneCode)){
            throw new BusinessException(TokenUtil.PHONE_NOT_CORRECT);
        }

        UserInfo userInfo = null;
        if (Fc.isNotBlank(phone)) {

            if (!Fc.isNull(authUserInfo)){
                return authUserInfo.getPhoneUserInfo(tokenParameter);
            }else {
                TbCoreUser result = UserCache.getUserByPhone(phone,tenantCode);
                return UserUtil.toUserInfo(result);
            }
        }
        return userInfo;
    }

}
