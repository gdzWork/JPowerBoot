package top.jpower.jpower.service.core.role;

import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleData;
import top.jpower.jpower.module.common.service.BaseService;

/**
 * @author ding
 * @description
 * @date 2020-11-04 11:02
 */
public interface CoreRoleDataService extends BaseService<TbCoreRoleData> {
}
