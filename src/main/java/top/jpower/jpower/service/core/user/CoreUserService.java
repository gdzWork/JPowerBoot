package top.jpower.jpower.service.core.user;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import top.jpower.jpower.dbs.entity.core.user.TbCoreUser;
import top.jpower.jpower.module.common.service.BaseService;
import top.jpower.jpower.vo.core.UserVo;

import java.util.List;

/**
 * @author mr.gmac
 */
public interface CoreUserService extends BaseService<TbCoreUser> {

    /**
     * @Author 郭丁志
     * @Description //TODO 查询用户列表
     * @Date 17:30 2020-05-18
     * @Param [coreParam]
     * @return java.util.List<top.jpower.jpower.module.dbs.entity.core.user.TbCoreUser>
     **/
    PageInfo<UserVo> listPage(TbCoreUser coreUser);

    /**
     * @Author 郭丁志
     * @Description //TODO 新增登录用户
     * @Date 2020-05-19
     * @Param [coreUser]
     * @return java.lang.Integer
     **/
    @Override
    boolean save(TbCoreUser coreUser);

    /**
     * @Author 郭丁志
     * @Description //TODO 批量删除用户
     * @Date 11:28 2020-05-19
     * @Param [ids]
     * @return java.lang.Integer
     **/
    Boolean delete(List<Long> ids);

    /**
     * @Author 郭丁志
     * @Description //TODO 修改用户
     * @Date 11:36 2020-05-19
     * @Param [coreUser]
     * @return java.lang.Integer
     **/
    Boolean update(TbCoreUser coreUser);

    /**
     * @Author 郭丁志
     * @Description //TODO 该用户名已存在
     * @Date 17:20 2020-05-19
     * @Param [loginId]
     * @return top.jpower.jpower.module.dbs.entity.core.user.TbCoreUser
     **/
    TbCoreUser selectUserLoginId(String loginId, String tenantCode);

    /**
     * @author 郭丁志
     * @Description //TODO 通过id查询用户信息
     * @date 1:27 2020/5/24 0024
     * @param id 用户id
     * @return top.jpower.jpower.module.dbs.entity.core.user.TbCoreUser
     */
    UserVo selectUserById(Long id);

    /**
     * @author 郭丁志
     * @Description //TODO 修改用户密码
     * @date 1:28 2020/5/24 0024
     * @param ids 用户id
     * @param pass 用户加密后密码
     * @return java.lang.Integer
     */
    Boolean updateUserPassword(List<Long> ids, String pass);

    /**
     * @author 郭丁志
     * @Description //TODO 批量新增
     * @date 2:55 2020/5/24 0024
     * @param list
     * @return java.lang.Integer
     */
    boolean insertBatch(List<TbCoreUser> list, boolean isCover);

    /**
     * @author 郭丁志
     * @Description //TODO 更新用户角色
     * @date 0:44 2020/5/25 0025
     * @param userIds 用户ID 多个逗号分隔
     * @param roleIds  角色ID 多个逗号分隔
     * @return java.lang.Integer
     */
    Boolean updateUsersRole(List<Long> userIds, List<Long> roleIds);

    /**
     * @Author 郭丁志
     * @Description //TODO 通过手机号查找用户
     * @Date 10:38 2020-07-03
     * @Param [phone]
     * @return top.jpower.jpower.module.dbs.entity.core.user.TbCoreUser
     **/
    TbCoreUser selectByPhone(String phone, String tenantCode);

    /**
     * @author 郭丁志
     * @Description //TODO 更新用户登陆信息
     * @date 0:02 2020/10/21 0021
     * @param userId
     */
    Boolean updateLoginInfo(Long userId);

    /**
     * @Author 郭丁志
     * @Description //TODO 查询用户列表
     * @Date 14:49 2020-08-20
     * @Param [coreUser, orgCode]
     **/
    List<UserVo> list(TbCoreUser coreUser);

    /**
     * @author 郭丁志
     * @Description //TODO 通过第三方验证码查询用户
     * @date 0:08 2020/10/21 0021
     * @param otherCode
     * @param tenantCode
     */
    TbCoreUser selectUserByOtherCode(String otherCode, String tenantCode);

    /**
     * @author 郭丁志
     * @Description // 创建管理员用户
     * @date 0:09 2020/10/25 0025
     * @param user 用户信息
     * @param roleId 角色ID
     */
    boolean saveUser(TbCoreUser user, Long roleId);

    boolean addRoleUsers(Long roleId, List<Long> userIds);

    boolean deleteRoleUsers(Long roleId, List<Long> userIds);

    UserVo getById(Long id);

    Page<UserVo> page(Page<TbCoreUser> page, Wrapper<TbCoreUser> queryWrapper);

    boolean validatePassword(String account, String password, String tenantCode);

    /**
     * 修改用户手机号
     *
     * @author mr.g
     * @param userId 用户ID
     * @param phone 要修改的手机号
     * @return 是否成功
     **/
    boolean updatePhone(String phone, Long userId);

    /**
     * 修改用户邮箱
     *
     * @author mr.g
     * @param email 邮箱
     * @param userId 用户ID
     * @return 是否成功
     **/
    boolean updateEmail(String email, Long userId);
}
