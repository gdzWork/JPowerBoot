package top.jpower.jpower.service.core.role.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import top.jpower.jpower.dbs.dao.core.role.TbCoreRoleFunctionDao;
import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreRoleFunctionMapper;
import top.jpower.jpower.dbs.entity.core.function.TbCoreFunction;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleFunction;
import top.jpower.jpower.module.common.redis.RedisUtil;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.module.common.support.ChainMap;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.module.common.utils.constants.ConstantsEnum;
import top.jpower.jpower.module.mp.support.Condition;
import top.jpower.jpower.service.core.role.CoreFunctionService;
import top.jpower.jpower.service.core.role.CoreRoleFunctionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author mr.gmac
 */
@AllArgsConstructor
@Service("coreRoleFunctionService")
public class CoreRoleFunctionServiceImpl extends BaseServiceImpl<TbCoreRoleFunctionMapper, TbCoreRoleFunction> implements CoreRoleFunctionService {

    public TbCoreRoleFunctionDao coreRoleFunctionDao;
    public CoreFunctionService coreFunctionService;
    public RedisUtil redisUtil;

    @Override
    public List<Map<String,Object>> selectRoleFunctionByRoleId(Long roleId) {
        return coreRoleFunctionDao.getBaseMapper().selectRoleFunctionByRoleId(roleId);
    }

    @Override
    public boolean addRoleFunctions(Long roleId, List<Long> functionIds, boolean isAutoSaveInterface) {

        //先删除角色原有权限
        coreRoleFunctionDao.removeRealByMap(ChainMap.<String,Object>create().put("role_id",roleId).build());

        //把下级的接口权限自动给
        if (isAutoSaveInterface){
            List<Long> fIds = coreFunctionService.listObjs(Condition.<TbCoreFunction>getQueryWrapper().lambda()
                    .select(TbCoreFunction::getId)
                    .eq(TbCoreFunction::getFunctionType, ConstantsEnum.FUNCTION_TYPE.INTERFACE.getValue())
                    .in(TbCoreFunction::getParentId, functionIds), Fc::toLong);
            if (Fc.isNotEmpty(fIds)){
                functionIds.addAll(fIds);
            }
        }

        List<TbCoreRoleFunction> roleFunctions = new ArrayList<>();
        if (Fc.notNull(functionIds)){
            for (Long fId : functionIds) {
                TbCoreRoleFunction roleFunction = new TbCoreRoleFunction();
                roleFunction.setId(Fc.randomSnowFlakeId());
                roleFunction.setFunctionId(fId);
                roleFunction.setRoleId(roleId);
                roleFunctions.add(roleFunction);
            }
        }

        if (roleFunctions.size() > 0){
            return coreRoleFunctionDao.saveBatch(roleFunctions);
        }
        return true;
    }

}
