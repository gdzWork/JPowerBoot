package top.jpower.jpower.service.core.dict.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.jpower.jpower.dbs.dao.core.dict.TbCoreDictDao;
import top.jpower.jpower.dbs.dao.core.dict.mapper.TbCoreDictMapper;
import top.jpower.jpower.dbs.entity.core.dict.TbCoreDict;
import top.jpower.jpower.module.base.enums.JpowerError;
import top.jpower.jpower.module.base.exception.JpowerAssert;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.module.common.utils.ShieldUtil;
import top.jpower.jpower.module.common.utils.constants.ConstantsEnum;
import top.jpower.jpower.module.mp.support.Condition;
import top.jpower.jpower.service.core.dict.CoreDictService;
import top.jpower.jpower.vo.core.DictVo;

import java.util.List;
import java.util.Map;

import static top.jpower.jpower.module.common.utils.constants.JpowerConstants.TOP_CODE;
import static top.jpower.jpower.module.tenant.TenantConstant.DEFAULT_TENANT_CODE;

/**
 * @author mr.gmac
 */
@Service("coreDictService")
public class CoreDictServiceImpl extends BaseServiceImpl<TbCoreDictMapper, TbCoreDict> implements CoreDictService {

    @Autowired
    private TbCoreDictDao dictDao;

    @Override
    public TbCoreDict queryDictTypeByCode(String dictTypeCode, String code) {
        LambdaQueryWrapper<TbCoreDict> wrapper = Condition.<TbCoreDict>getQueryWrapper().lambda()
                .eq(TbCoreDict::getDictTypeCode,dictTypeCode)
                .eq(TbCoreDict::getCode,code);
        if(ShieldUtil.isRoot()){
            wrapper.eq(TbCoreDict::getTenantCode,DEFAULT_TENANT_CODE);
        }
        return dictDao.getOne(wrapper);
    }

    @Override
    public Boolean saveDict(TbCoreDict dict) {
        TbCoreDict coreDictType = queryDictTypeByCode(dict.getDictTypeCode(),dict.getCode());
        if(Fc.isNull(dict.getId())){
            dict.setLocale(Fc.isBlank(dict.getLocale())? ConstantsEnum.YYZL.CHINA.getValue() :dict.getLocale());
            dict.setIsStop(Fc.isBlank(dict.getIsStop())? ConstantsEnum.YN.N.getValue() : dict.getIsStop());
            dict.setParentId(Fc.notNull(dict.getParentId())?dict.getParentId():Fc.toLong(TOP_CODE));
            JpowerAssert.notTrue(coreDictType != null, JpowerError.Business,"该字典已存在");
        }else {
            JpowerAssert.notTrue(coreDictType != null && !Fc.equalsValue(dict.getId(),coreDictType.getId()), JpowerError.Business,"该字典已存在");
        }

        return dictDao.saveOrUpdate(dict);
    }

    @Override
    public List<DictVo> listByType(TbCoreDict dict) {
        if (ShieldUtil.isRoot()) {
            dict.setTenantCode(Fc.isBlank(dict.getTenantCode()) ? DEFAULT_TENANT_CODE : dict.getTenantCode());
        } else {
            dict.setTenantCode(ShieldUtil.getTenantCode());
        }
        return dictDao.getBaseMapper().listByType(dict);
    }

    @Override
    public List<Map<String, Object>> listByTypeCode(String dictTypeCode) {
        LambdaQueryWrapper<TbCoreDict> queryWrapper = Condition.<TbCoreDict>getQueryWrapper().lambda()
                .select(TbCoreDict::getCode,TbCoreDict::getName,TbCoreDict::getLocale)
                .eq(TbCoreDict::getDictTypeCode,dictTypeCode);
        if (ShieldUtil.isRoot()){
            queryWrapper.eq(TbCoreDict::getTenantCode,DEFAULT_TENANT_CODE);
        }
        //这里不能返回实体类，不然会造成字典回写的死循环
        return dictDao.listMaps(queryWrapper);
    }

}
