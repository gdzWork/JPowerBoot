package top.jpower.jpower.service.core.role.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top.jpower.jpower.dbs.dao.core.role.TbCoreFunctionDao;
import top.jpower.jpower.dbs.dao.core.role.TbCoreRoleDao;
import top.jpower.jpower.dbs.dao.core.role.TbCoreRoleFunctionDao;
import top.jpower.jpower.dbs.dao.core.role.TbCoreRoleMenuDao;
import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreRoleMapper;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRole;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleMenu;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.module.mp.support.Condition;
import top.jpower.jpower.service.core.role.CoreRoleService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mr.gmac
 */
@Service("coreRoleService")
@RequiredArgsConstructor
public class CoreRoleServiceImpl extends BaseServiceImpl<TbCoreRoleMapper, TbCoreRole> implements CoreRoleService {

    private final TbCoreRoleDao coreRoleDao;
    private final TbCoreRoleFunctionDao coreRoleFunctionDao;
    private final TbCoreFunctionDao coreFunctionDao;
    private final TbCoreRoleMenuDao coreRoleMenuDao;

    @Override
    public Boolean add(TbCoreRole coreRole) {
        if (coreRoleDao.save(coreRole)){
            List<Long> functionIds = coreFunctionDao.queryIdByTopChild();
            return coreRoleFunctionDao.saveFunctions(functionIds,coreRole.getId());
        }
        return false;
    }

    @Override
    public long listByPids(List<Long> ids) {
        return coreRoleDao.count(Condition.<TbCoreRole>getQueryWrapper().lambda().in(TbCoreRole::getParentId, ids).notIn(TbCoreRole::getId, ids));
    }

    @Override
    public Boolean update(TbCoreRole coreRole) {
        return coreRoleDao.updateById(coreRole);
    }

    @Override
    public boolean saveTopMenu(Long roleId, List<Long> menuIds) {

        coreRoleMenuDao.removeReal(Condition.<TbCoreRoleMenu>getQueryWrapper().lambda().eq(TbCoreRoleMenu::getRoleId,roleId));

        if (Fc.isEmpty(menuIds)){
            return true;
        }

        List<TbCoreRoleMenu> roleMenuList = new ArrayList<>();
        menuIds.forEach(id->{
            TbCoreRoleMenu roleMenu = new TbCoreRoleMenu();
            roleMenu.setRoleId(roleId);
            roleMenu.setMenuId(id);
            roleMenuList.add(roleMenu);
        });

        return coreRoleMenuDao.addBatchSomeColumn(roleMenuList);
    }

    @Override
    public List<Long> topMenuId(Long roleId) {
        return coreRoleMenuDao.listObjs(Condition.<TbCoreRoleMenu>getQueryWrapper()
                .lambda()
                .select(TbCoreRoleMenu::getMenuId)
                .eq(TbCoreRoleMenu::getRoleId,roleId), Fc::toLong);
    }
}
