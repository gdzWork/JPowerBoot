package top.jpower.jpower.service.core.role;

import cn.hutool.core.lang.tree.Tree;
import top.jpower.jpower.dbs.entity.core.function.TbCoreFunction;
import top.jpower.jpower.module.common.service.BaseService;
import top.jpower.jpower.vo.core.DataFunctionVo;
import top.jpower.jpower.vo.core.FunctionVo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author mr.gmac
 */
public interface CoreFunctionService extends BaseService<TbCoreFunction> {

    /**
     * 查询功能层级树
     *
     * @author mr.g
     * @param userRole
     * @param clientId
     * @return java.util.List<cn.hutool.core.lang.tree.Tree<java.lang.String>>
     **/
    List<Tree<Long>> treeMenuTypeByClientId(List<Long> userRole, Long clientId);

    /**
     * @Author 郭丁志
     * @Description //TODO map查询
     * @Date 15:33 2020-05-20
     * @Param [coreFunction]
     * @return java.util.List<top.jpower.jpower.module.dbs.entity.core.function.TbCoreFunction>
     **/
    List<FunctionVo> listFunction(Map<String, Object> coreFunction);

    /**
     * @Author 郭丁志
     * @Description //TODO 通过code查询菜单
     * @Date 15:34 2020-05-20
     * @Param [code]
     * @return top.jpower.jpower.module.dbs.entity.core.function.TbCoreFunction
     **/
    TbCoreFunction selectFunctionByCode(String code);

    /**
     * @Author 郭丁志
     * @Description //TODO 通过url查询菜单
     * @Date 15:35 2020-05-20
     * @Param [url]
     * @return top.jpower.jpower.module.dbs.entity.core.function.TbCoreFunction
     **/
    TbCoreFunction selectFunctionByUrl(String url);

    /**
     * @Author 郭丁志
     * @Description //TODO 新增菜单
     * @Date 15:35 2020-05-20
     * @Param [coreFunction]
     * @return java.lang.Integer
     **/
    Boolean add(TbCoreFunction coreFunction);

    /**
     * @Author 郭丁志
     * @Description //TODO 批量查询id下的子节点数量
     * @Date 15:40 2020-05-20
     * @Param [ids]
     * @return java.lang.Integer
     **/
    long listByPids(List<Long> ids);

    /**
     * @Author 郭丁志
     * @Description //TODO 删除菜单
     * @Date 15:43 2020-05-20
     * @Param [ids]
     * @return java.lang.Integer
     **/
    Boolean delete(List<Long> ids);

    /**
     * @Author 郭丁志
     * @Description //TODO 修改菜单
     * @Date 11:19 2020-07-16
     * @Param [coreFunction]
     * @return java.lang.Integer
     **/
    Boolean update(TbCoreFunction coreFunction);

    /**
     * 保存层级
     *
     * @author mr.g
     * @param parentId 上级ID
     * @param ids 主键
     * @return boolean
     **/
    boolean hierarchySave(Long parentId, List<Long> ids);

    /**
     * @author 郭丁志
     * @Description //TODO 查询角色所有权限ID
     * @date 22:50 2020/7/26 0026
     * @param roleIds
     * @return java.util.List<top.jpower.jpower.module.common.node.Node>
     */
    Set<Long> queryUrlIdByRole(List<Long> roleIds);

    /**
     * @author 郭丁志
     * @Description //TODO 懒加载角色所有功能
     * @date 23:06 2020/7/26 0026
     * @param parentId
     * @param roleIds
     * @return java.util.List<top.jpower.jpower.module.common.node.Node>
     */
    List<Tree<Long>> lazyTreeByRole(Long parentId, List<Long> roleIds);

    /**
     * @Author mr.g
     * @Description //TODO 根据角色ID查询所有菜单
     * @Date 11:23 2020-07-30
     * @Param [roleIds]
     * @return java.util.List<top.jpower.jpower.module.dbs.entity.core.function.TbCoreFunction>
     **/
    List<TbCoreFunction> listMenuByRoleId(List<Long> roleIds, String clientCode, Long topMenuId, boolean isHide);

    /**
     * @Author mr.g
     * @Description //TODO 根据角色，查询一个角色下的所有可用按钮
     * @Date 11:38 2020-07-30
     * @Param [roleIds]
     * @return java.util.List<top.jpower.jpower.module.dbs.entity.core.function.TbCoreFunction>
     **/
    List<String> listBtnByRoleId(List<Long> roleIds);

    /**
     * @Author 郭丁志
     * @Description //TODO 查询菜单list树形结构
     * @Date 21:49 2020-07-30
     * @Param [roleIds, functionVoClass]
     * @return java.util.List<top.jpower.jpower.module.dbs.vo.FunctionVo>
     **/
    List<Tree<Long>> listTreeByRoleId(List<Long> roleIds);

    /**
     * 查询匿名用户是否拥有该URL的权限
     *
     * @author mr.g
     * @return long
     **/
    List<String> queryUrlAnonymousRole();

    List<String> getUrlsByRoleIds(List<Long> roleIds, String clientCode);

    /**
     * 查询树形菜单
     *
     * @author ding
     * @date 00:46 2021-02-27
     * @param roleIds 角色ID
     * @param clientId 客户端ID
     * @return java.util.List<top.jpower.jpower.module.common.node.Node>
     **/
    List<Tree<Long>> menuTreeByRoleIds(List<Long> roleIds,Long clientId, Long topMenuId);

    /**
     * 查询角色所有菜单
     *
     * @author mr.g
     * @param roleIds 角色ID
     * @return java.util.List<top.jpower.jpower.dbs.entity.function.TbCoreFunction>
     **/
    List<TbCoreFunction> menuByRoleIds(List<Long> roleIds);

    /**
     * 查询按钮
     * @author mr.g
     * @date 00:47 2021-02-27
     * @param roleIds 角色ID
     * @param id 菜单ID
     * @param clientId 客户端ID
     * @return java.util.List<top.jpower.jpower.dbs.entity.function.TbCoreFunction>
     **/
    List<Tree<Long>> treeButByMenu(List<Long> roleIds, Long id, Long clientId);

    /**
     * 生成功能点
     *
     * @author mr.g
     * @return boolean
     **/
    boolean generateFunction();

    List<DataFunctionVo> listDataFunction(Map<String, Object> coreFunction);

    /**
     * 客户端下的接口资源
     *
     * @author mr.g
     * @param roleIds 角色ID
     * @param clientId 客户端ID
     * @return 接口资源
     **/
    List<Map<String, Object>> listInterface(List<Long> roleIds, Long clientId);
}
