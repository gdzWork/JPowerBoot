package top.jpower.jpower.service.core.role.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import top.jpower.jpower.dbs.dao.core.client.TbCoreClientDao;
import top.jpower.jpower.dbs.dao.core.role.TbCoreDataScopeDao;
import top.jpower.jpower.dbs.dao.core.role.TbCoreRoleDataDao;
import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreDataScopeMapper;
import top.jpower.jpower.dbs.entity.core.function.TbCoreDataScope;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleData;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.module.common.utils.Fc;
import top.jpower.jpower.module.common.utils.StringUtil;
import top.jpower.jpower.module.common.utils.constants.ConstantsEnum;
import top.jpower.jpower.module.mp.support.Condition;
import top.jpower.jpower.service.core.role.CoreDataScopeService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ding
 * @description 数据权限业务
 * @date 2020-11-03 15:02
 */
@Service
@AllArgsConstructor
public class CoreDataScopeServiceImpl extends BaseServiceImpl<TbCoreDataScopeMapper, TbCoreDataScope> implements CoreDataScopeService {

    private TbCoreDataScopeDao dataScopeDao;
    private TbCoreRoleDataDao roleDataDao;
    private TbCoreClientDao clientDao;

    private final String sql = "select data_id from tb_core_role_data where role_id in ({})";

    @Override
    public boolean save(TbCoreDataScope dataScope){
        return dataScopeDao.save(dataScope);
    }

    @Override
    public boolean roleDataScope(Long roleId, List<Long> dataIds) {

        roleDataDao.removeReal(Condition.<TbCoreRoleData>getQueryWrapper().lambda()
                                .eq(TbCoreRoleData::getRoleId,roleId));
        if (dataIds.size() > 0){
            List<TbCoreRoleData> list = new ArrayList<>();
            dataIds.forEach(dataId -> {
                TbCoreRoleData roleData = new TbCoreRoleData();
                roleData.setDataId(dataId);
                roleData.setRoleId(roleId);
                list.add(roleData);
            });
            return roleDataDao.saveBatch(list);
        }
        return true;
    }

    @Override
    public List<TbCoreDataScope> getAllRoleDataScope() {
        return dataScopeDao.list(Condition.<TbCoreDataScope>getQueryWrapper().lambda().eq(TbCoreDataScope::getAllRole, ConstantsEnum.YN01.Y.getValue()));
    }

    @Override
    public List<TbCoreDataScope> getDataScopeByRole(List<Long> roleIds,String clientCode) {
        String inSql = Fc.join(roleIds);
        return dataScopeDao.list(Condition.<TbCoreDataScope>getQueryWrapper().lambda()
                .inSql(TbCoreDataScope::getMenuId,"select id from tb_core_function where client_id = " + clientDao.queryIdByCode(clientCode) + " and function_type = " + ConstantsEnum.FUNCTION_TYPE.MENU.getValue())
                .and(query-> query.inSql(TbCoreDataScope::getId, StringUtil.format(sql,inSql))
                        .or().eq(TbCoreDataScope::getAllRole,ConstantsEnum.YN01.Y.getValue())));
    }

    @Override
    public List<TbCoreDataScope> getDataScopeByRoleAndMenu(List<Long> roleIds,String menuCode) {
        String inSql = Fc.join(roleIds);
        List<TbCoreDataScope> list = dataScopeDao.list(Condition.<TbCoreDataScope>getQueryWrapper().lambda()
                .inSql(TbCoreDataScope::getMenuId,StringUtil.format("select id from tb_core_function where code = '{}'",menuCode))
                .and(query-> query.inSql(TbCoreDataScope::getId, StringUtil.format(sql,inSql)).or().eq(TbCoreDataScope::getAllRole,ConstantsEnum.YN01.Y.getValue())));
        return list.stream().sorted(Comparator.comparingInt(TbCoreDataScope::getAllRole)).collect(Collectors.toList());
    }

}
