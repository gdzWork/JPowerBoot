package top.jpower.jpower.service.core.role.impl;

import top.jpower.jpower.dbs.dao.core.role.mapper.TbCoreRoleDataMapper;
import top.jpower.jpower.dbs.entity.core.role.TbCoreRoleData;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.service.core.role.CoreRoleDataService;
import org.springframework.stereotype.Service;

/**
 * @author ding
 * @description
 * @date 2020-11-04 11:02
 */
@Service
public class CoreRoleDataServiceImpl extends BaseServiceImpl<TbCoreRoleDataMapper, TbCoreRoleData> implements CoreRoleDataService {
}
