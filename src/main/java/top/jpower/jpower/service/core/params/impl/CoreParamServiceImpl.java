package top.jpower.jpower.service.core.params.impl;

import top.jpower.jpower.dbs.dao.core.params.TbCoreParamsDao;
import top.jpower.jpower.dbs.dao.core.params.mapper.TbCoreParamsMapper;
import top.jpower.jpower.dbs.entity.core.params.TbCoreParam;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.service.core.params.CoreParamService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author mr.gmac
 */
@Service("coreParamService")
@AllArgsConstructor
public class CoreParamServiceImpl extends BaseServiceImpl<TbCoreParamsMapper, TbCoreParam> implements CoreParamService {

    private TbCoreParamsDao paramsDao;

    @Override
    public String selectByCode(String code) {
        return baseMapper.selectByCode(code);
    }

    @Override
    public Boolean update(TbCoreParam coreParam) {
        return paramsDao.updateById(coreParam);
    }

}
