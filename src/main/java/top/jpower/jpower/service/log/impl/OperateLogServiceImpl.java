package top.jpower.jpower.service.log.impl;

import top.jpower.jpower.dbs.dao.log.LogOperateDao;
import top.jpower.jpower.dbs.dao.log.mapper.LogOperateMapper;
import top.jpower.jpower.dbs.entity.log.TbLogOperate;
import top.jpower.jpower.module.base.model.OperateLogDto;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.module.common.utils.BeanUtil;
import top.jpower.jpower.service.log.OperateLogService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Author mr.g
 * @Date 2021/4/19 0019 19:15
 */
@Service
@AllArgsConstructor
public class OperateLogServiceImpl extends BaseServiceImpl<LogOperateMapper, TbLogOperate> implements OperateLogService {

    private LogOperateDao operateDao;

    @Override
    public void saveOperateLog(OperateLogDto operateLog) {
        TbLogOperate logOperate = BeanUtil.copyProperties(operateLog, TbLogOperate.class);
        operateDao.save(logOperate);
    }
}
