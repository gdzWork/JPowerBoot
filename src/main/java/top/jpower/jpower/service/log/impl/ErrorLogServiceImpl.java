package top.jpower.jpower.service.log.impl;

import top.jpower.jpower.dbs.dao.log.LogErrorDao;
import top.jpower.jpower.dbs.dao.log.mapper.LogErrorMapper;
import top.jpower.jpower.dbs.entity.log.TbLogError;
import top.jpower.jpower.module.base.model.ErrorLogDto;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.module.common.utils.BeanUtil;
import top.jpower.jpower.service.log.ErrorLogService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Author mr.g
 * @Date 2021/4/19 0019 19:15
 */
@Service
@AllArgsConstructor
public class ErrorLogServiceImpl extends BaseServiceImpl<LogErrorMapper, TbLogError> implements ErrorLogService {

    private LogErrorDao errorDao;

    @Override
    public void saveErrorLog(ErrorLogDto errorLog) {
        TbLogError logError = BeanUtil.copyProperties(errorLog, TbLogError.class);
        errorDao.save(logError);
    }
}
