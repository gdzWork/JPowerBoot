package top.jpower.jpower.service.log;

import top.jpower.jpower.dbs.entity.log.TbLogError;
import top.jpower.jpower.module.base.model.ErrorLogDto;
import top.jpower.jpower.module.common.service.BaseService;

/**
 * @Author mr.g
 * @Date 2021/4/19 0019 19:15
 */
public interface ErrorLogService extends BaseService<TbLogError> {

    void saveErrorLog(ErrorLogDto errorLog);
}
