package top.jpower.jpower.service.log;

import top.jpower.jpower.dbs.entity.log.TbLogOperate;
import top.jpower.jpower.module.base.model.OperateLogDto;
import top.jpower.jpower.module.common.service.BaseService;

/**
 * @Author mr.g
 * @Date 2021/4/19 0019 19:15
 */
public interface OperateLogService extends BaseService<TbLogOperate> {

    void saveOperateLog(OperateLogDto operateLog);
}
