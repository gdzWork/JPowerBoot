package top.jpower.jpower.service.resource.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top.jpower.jpower.dbs.dao.resource.TbResourceSmsDao;
import top.jpower.jpower.dbs.dao.resource.mapper.TbResourceSmsMapper;
import top.jpower.jpower.dbs.entity.resource.TbResourceSms;
import top.jpower.jpower.module.common.service.impl.BaseServiceImpl;
import top.jpower.jpower.service.resource.ResourceSmsService;

/**
 * <p>
 * 短信配置表 服务实现类
 * </p>
 *
 * @author mr.g
 * @since 2024-03-04
 */
@Service
@RequiredArgsConstructor
public class ResourceSmsServiceImpl extends BaseServiceImpl<TbResourceSmsMapper, TbResourceSms> implements ResourceSmsService {

    private final TbResourceSmsDao resourceSmsDao;

    /**
     * 通过code查询
     *
     * @param code
     * @return
     * @author mr.g
     **/
    @Override
    public TbResourceSms getByCode(String code) {
        return resourceSmsDao.getByCode(code);
    }
}
