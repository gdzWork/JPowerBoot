package top.jpower.jpower.service.resource;

import top.jpower.jpower.dbs.entity.resource.TbResourceOss;
import top.jpower.jpower.module.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * @author mr.g
 * @date 2024/4/23 3:03 PM
 */
public interface ResourceOssService extends BaseService<TbResourceOss> {

    /**
     * 列表
     *
     * @author mr.g
     * @param
     * @return
     **/
    List<Map<String, Object>> listCodeName();

}
